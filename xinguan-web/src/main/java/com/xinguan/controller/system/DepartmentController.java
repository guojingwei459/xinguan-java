package com.xinguan.controller.system;

import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.service.TbDepartmentService;
import com.xinguan.vo.system.DepartmentVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author 郭经伟
 * @Date 2021/7/11
 * <p>
 * 部门管理有下面这些接口
 * 1. 查询部门列表
 * 2. 新增部门
 * 3. 编辑部门
 * 4. 删除部门
 **/
@RestController
@RequestMapping("/system/department")
@Api(tags = "系统模块-部门相关接口")
public class DepartmentController {


    @Resource
    private TbDepartmentService departmentService;


    @ApiOperation(value = "查询部门列表")
    @GetMapping(value = {"findDepartmentList/{currentPage}/{pageSize}","findDepartmentList/{currentPage}/{pageSize}/{departmentName}"})
    public ResponseBean findDepartmentList(@PathVariable("currentPage") Integer currentPage,
                                           @PathVariable("pageSize") Integer pageSize,
                                           @PathVariable( required = false,value = "departmentName") String departmentName) {
        ResponseBean result=departmentService.findDepartmentList(currentPage,pageSize,departmentName);
        return result;
    }

    @ApiOperation(value = "查询所有部门名")
    @GetMapping("findAllDepartmentNames")
    public ResponseBean findAllDepartmentNames()  {
        ResponseBean result=departmentService.findAllDepartmentNames();
        return result;
    }

    @ApiOperation(value = "新增部门")
    @PostMapping("addDepartment")
    public ResponseBean addDepartment(@Validated @RequestBody DepartmentVO departmentVO) throws SystemException {
        departmentService.addDepartment(departmentVO);
        return ResponseBean.success();
    }

    @ApiOperation(value = "删除部门")
    @DeleteMapping("deleteDepartment/{id}")
    public ResponseBean deleteDepartment(@PathVariable("id")Long id) throws SystemException {
        departmentService.deleteDepartment(id);
        return ResponseBean.success();
    }

    @ApiOperation(value = "查找编辑的部门")
    @GetMapping("edit/{id}")
    public ResponseBean edit(@PathVariable("id") Long id) throws SystemException {
        DepartmentVO departmentVO = departmentService.edit(id);
        return ResponseBean.success(departmentVO);
    }

    @ApiOperation(value = "编辑部门")
    @PutMapping("updateRole/{id}")
    public ResponseBean updateDepartment(@PathVariable("id") Long id, @Validated @RequestBody DepartmentVO departmentVO) throws SystemException {
        departmentService.updateDepartment(id, departmentVO);
        return ResponseBean.success();
    }
}
