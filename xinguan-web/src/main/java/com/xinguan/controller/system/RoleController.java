package com.xinguan.controller.system;

import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.service.TbRoleService;
import com.xinguan.vo.system.RoleVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * @author 郭经伟
 * @Date 2021/7/11
 * 角色相关有如下接口
 * 1. 条件查询角色列表 ok
 * 2. 新增角色 ok
 * 3. 删除角色 ok
 * 4. 编辑角色 ok
 * 5. 角色授权接口
 * 6. 启用或者禁用指定角色 ok
 **/
@RestController
@RequestMapping("/system/role")
@Api(tags = "系统模块-角色相关接口")
public class RoleController {


    @Autowired
    private TbRoleService roleService;

    /**
     * 条件化查询role列表
     *
     * @param currentPage
     * @param pageSize
     * @param roleName
     * @return
     */
    @ApiOperation(value = "角色列表")
    @GetMapping(value = {"findRoleList/{currentPage}/{pageSize}/","findRoleList/{currentPage}/{pageSize}/{roleName}"})
    public ResponseBean findRoleList(@PathVariable("currentPage") Integer currentPage,
                                     @PathVariable("pageSize") Integer pageSize,
                                     @PathVariable(required = false,value = "roleName") String roleName) {
        ResponseBean result = roleService.findRoleList(currentPage, pageSize, roleName);
        return result;
    }

    /**
     * 添加角色信息
     *
     * @param roleVO
     * @return
     */
    @ApiOperation(value = "添加角色")
    @PostMapping("addRole")
    public ResponseBean addRole(@Validated @RequestBody RoleVO roleVO) throws SystemException {
        roleService.addRole(roleVO);
        return ResponseBean.success();
    }

    /**
     * 删除角色
     *
     * @param id
     * @return
     */
    @ApiOperation(value = "删除角色")
    @DeleteMapping("deleteRole/{id}")
    public ResponseBean deleteRole(@PathVariable("id") Long id) throws SystemException {
        roleService.deleteRole(id);
        return ResponseBean.success();
    }


    @ApiOperation(value = "查找编辑的角色")
    @GetMapping("edit/{id}")
    public ResponseBean edit(@PathVariable("id") Long id) throws SystemException {
        RoleVO roleVO = roleService.edit(id);
        return ResponseBean.success(roleVO);
    }

    @ApiOperation(value = "编辑角色")
    @PutMapping("updateRole/{id}")
    public ResponseBean updateRole(@PathVariable("id") Long id, @Validated @RequestBody RoleVO roleVO) throws SystemException {
        roleService.updateOneRole(id, roleVO);
        return ResponseBean.success();
    }

    @ApiOperation(value = "更新状态")
    @PutMapping("updateRoleStatus/{id}/{status}")
    public ResponseBean updateRoleStatus(@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws SystemException {
        roleService.updateRoleStatus(id, status);
        return ResponseBean.success();
    }
}
