package com.xinguan.controller.system;

import com.xinguan.annotation.ControllerEndPoint;
import com.xinguan.dto.UserLoginDTO;
import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.service.TbUserService;
import com.xinguan.vo.system.UserEditVO;
import com.xinguan.vo.system.UserInfoVO;
import com.xinguan.vo.system.UserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @author 郭经伟
 * @Date 2021/7/10
 * <p>
 * 系统用户相关的接口
 * 1. 用户登录接口
 * 2. 条件查询用户接口
 * 3. 新增用户接口
 * 4. 编辑用户接口
 * 5. 删除用户接口
 * 6. 改变用户状态接口
 **/
@RestController
@RequestMapping("/system/user")
@Validated
@Api(tags = "系统模块-用户相关接口")
public class UserController {

    @Autowired
    private TbUserService userService;


    /**
     * 用户登入
     *
     * @param userLoginDTO
     * @param request
     * @return
     */
    @ApiOperation(value = "用户登入", notes = "接收参数用户名和密码,登入成功后,返回JWTToken")
    @PostMapping("login")
    public ResponseBean<String> login(@RequestBody UserLoginDTO userLoginDTO, HttpServletRequest request) throws SystemException {
        String token = userService.login(userLoginDTO.getUsername(), userLoginDTO.getPassword());
        return ResponseBean.success(token);
    }

    @ControllerEndPoint(exceptionMessage = "查询用户列表失败", operation = "查询用户列表")
    @ApiOperation(value = "用户列表", notes = "模糊查询所有分页用户列表")
    @PostMapping("findUserList/{currentPage}/{pageSize}")
    public ResponseBean findUserList(@PathVariable("currentPage") Integer currentPage,
                                     @PathVariable("pageSize") Integer pageSize,
                                     @RequestBody UserVO userVO) {
        ResponseBean result = userService.findUserList(currentPage, pageSize, userVO);
        return ResponseBean.success(result);

    }

    /**
     * 添加用户
     *
     * @param userVO
     * @return
     */
    @ControllerEndPoint(exceptionMessage = "添加用户失败", operation = "添加用户")
    @ApiOperation(value = "添加用户")
    @PostMapping("addUser")
    public ResponseBean addUser(@Validated @RequestBody UserVO userVO) throws SystemException {
        userService.addUser(userVO);
        return ResponseBean.success();
    }

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @ControllerEndPoint(exceptionMessage = "删除用户失败", operation = "删除用户")
    @ApiOperation(value = "删除用户")
    @DeleteMapping("deleteUser/{id}")
    public ResponseBean deleteUser(@PathVariable("id") Long id) throws SystemException {
        userService.deleteUser(id);
        return ResponseBean.success();
    }


    @ControllerEndPoint(exceptionMessage = "查找编辑的角色失败", operation = "查找编辑的角色")
    @ApiOperation(value = "查找编辑的角色")
    @GetMapping("edit/{id}")
    public ResponseBean edit(@PathVariable("id") Long id) throws SystemException {
        UserEditVO userEditVO = userService.edit(id);
        return ResponseBean.success(userEditVO);
    }

    @ControllerEndPoint(exceptionMessage = "编辑角色失败", operation = "编辑角色")
    @ApiOperation(value = "编辑角色")
    @PutMapping("updateUser/{id}")
    public ResponseBean updateUser(@PathVariable("id") Long id, @Validated @RequestBody UserEditVO userEditVO) throws SystemException {
        userService.updateOneUser(id, userEditVO);
        return ResponseBean.success();
    }

    @ControllerEndPoint(exceptionMessage = "更新状态失败", operation = "更新状态")
    @ApiOperation(value = "更新状态")
    @PutMapping("updateUserStatus/{id}/{status}")
    public ResponseBean updateUserStatus(@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws SystemException {
        userService.updateUserStatus(id, status);
        return ResponseBean.success();
    }


    /**
     * 用户信息
     *
     * @return
     */
    @ControllerEndPoint(exceptionMessage = "查询用户信息失败", operation = "查询用户信息")
    @ApiOperation(value = "用户信息", notes = "用户登入信息")
    @GetMapping("/info")
    public ResponseBean info() {
        UserInfoVO userInfoVO = userService.info();
        return ResponseBean.success(userInfoVO);
    }


    /**
     * 分配角色
     *
     * @param id
     * @param rids
     * @return
     */
    @ControllerEndPoint(exceptionMessage = "分配角色失败", operation = "分配角色")
    @ApiOperation(value = "分配角色", notes = "角色分配给用户")
    @PostMapping("assignRoles/{id}")
    public ResponseBean assignRoles(@PathVariable("id") Long id, @RequestBody Long[] rids) throws SystemException {
        userService.assignRoles(id, rids);
        return ResponseBean.success();
    }
}
