package com.xinguan.controller.system;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 操作日志表 前端控制器
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-24
 */
@RestController
@RequestMapping("/tbLog")
public class TbLogController {

}
