package com.xinguan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author 郭经伟
 * @Date 2021/7/4
 **/
@SpringBootApplication
// 开启事务管理
@EnableTransactionManagement
@MapperScan("com.xinguan.dao")
public class XinguanApplication {

    public static void main(String[] args) {
        SpringApplication.run(XinguanApplication.class, args);
    }
}
