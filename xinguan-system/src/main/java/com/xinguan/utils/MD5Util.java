package com.xinguan.utils;

import org.apache.shiro.crypto.hash.SimpleHash;

/**
 * @author 郭经伟
 * @Date 2021/7/10
 **/
public class MD5Util {


    /**
     * 密码加密
     *
     * @param source
     * @param salt
     * @return
     */
    public static String md5Encryption(String source, String salt) {
        String algorithmName = "MD5";//加密算法
        int hashIterations = 1024;//hash散列次数
        SimpleHash simpleHash = new SimpleHash(algorithmName, source, salt, hashIterations);
        return simpleHash + "";
    }

}
