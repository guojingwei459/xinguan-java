package com.xinguan.shiro;

import org.apache.shiro.authc.AuthenticationToken;

/**
 * @author 郭经伟
 * @Date 2021/7/10
 **/
public class JWTToken implements AuthenticationToken {


    // 密匙
    private String token;

    public JWTToken(String token){
        this.token=token;
    }

    @Override
    public Object getPrincipal() {
        return token;
    }

    @Override
    public Object getCredentials() {
        return token;
    }
}
