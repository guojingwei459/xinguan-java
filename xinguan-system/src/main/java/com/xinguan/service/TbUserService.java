package com.xinguan.service;

import com.xinguan.entity.TbUser;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.vo.system.UserEditVO;
import com.xinguan.vo.system.UserInfoVO;
import com.xinguan.vo.system.UserVO;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-10
 */
public interface TbUserService extends IService<TbUser> {

    /**
     * 用户登入
     * @param username
     * @param password
     * @return
     */
    String login(String username,String password) throws SystemException;

    ResponseBean findUserList(Integer currentPage, Integer pageSize, UserVO userVO);

    void addUser(UserVO userVO) throws SystemException;

    void deleteUser(Long id) throws SystemException;

    UserEditVO edit(Long id) throws SystemException;

    void updateOneUser(Long id, UserEditVO userEditVO) throws SystemException;

    void updateUserStatus(Long id, Boolean status) throws SystemException;

    UserInfoVO info();

    void assignRoles(Long id, Long[] rids) throws SystemException;

}
