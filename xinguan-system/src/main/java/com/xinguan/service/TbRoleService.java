package com.xinguan.service;

import com.xinguan.entity.TbRole;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.vo.system.RoleVO;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-11
 */
public interface TbRoleService extends IService<TbRole> {

    ResponseBean findRoleList(Integer currentPage, Integer pageSize, String roleName);

    void addRole(RoleVO roleVO) throws SystemException;

    void deleteRole(Long id) throws SystemException;

    RoleVO edit(Long id) throws SystemException;

    void updateOneRole(Long id, RoleVO roleVO) throws SystemException;

    void updateRoleStatus(Long id, Boolean status) throws SystemException;

}
