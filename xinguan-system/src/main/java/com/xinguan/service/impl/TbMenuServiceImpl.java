package com.xinguan.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinguan.dao.TbMenuDao;
import com.xinguan.entity.TbMenu;
import com.xinguan.service.TbMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-22
 */
@Service
public class TbMenuServiceImpl extends ServiceImpl<TbMenuDao, TbMenu> implements TbMenuService {

}
