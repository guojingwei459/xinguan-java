package com.xinguan.service.impl;

import com.xinguan.dao.TbLogDao;
import com.xinguan.entity.TbLog;
import com.xinguan.service.TbLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 操作日志表 服务实现类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-24
 */
@Service
public class TbLogServiceImpl extends ServiceImpl<TbLogDao, TbLog> implements TbLogService {

}
