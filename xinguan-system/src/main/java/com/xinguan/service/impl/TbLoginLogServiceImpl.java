package com.xinguan.service.impl;

import com.xinguan.dao.TbLoginLogDao;
import com.xinguan.entity.TbLoginLog;
import com.xinguan.service.TbLoginLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 登录日志表 服务实现类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-24
 */
@Service
public class TbLoginLogServiceImpl extends ServiceImpl<TbLoginLogDao, TbLoginLog> implements TbLoginLogService {

}
