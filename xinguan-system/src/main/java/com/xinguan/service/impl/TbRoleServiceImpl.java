package com.xinguan.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinguan.dao.TbRoleDao;
import com.xinguan.entity.TbRole;
import com.xinguan.enums.system.RoleStatusEnum;
import com.xinguan.error.SystemCodeEnum;
import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.service.TbRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.xinguan.vo.system.RoleVO;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-11
 */
@Service
public class TbRoleServiceImpl extends ServiceImpl<TbRoleDao, TbRole> implements TbRoleService {


    @Resource
    private TbRoleDao roleDao;

    @Override
    public ResponseBean findRoleList(Integer currentPage, Integer pageSize, String roleName) {
        IPage<RoleVO> rolePageVo = roleDao.selectRolePageVo(new Page<RoleVO>(currentPage, pageSize), roleName);
        return ResponseBean.success(rolePageVo);
    }

    /**
     * 添加角色
     *
     * @param roleVO
     * @return
     */
    @Transactional
    @Override
    public void addRole(RoleVO roleVO) throws SystemException {
        // 添加角色的时候需要先判断是否当前角色存在 如果当前角色存在 就抛出自定义异常
        @NotBlank(message = "角色名必填") String roleName = roleVO.getRoleName();
        int count = roleDao.selectCount(new QueryWrapper<TbRole>().eq("role_name", roleName));
        if (count != 0) {
            // 当前添加的角色已经存在
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR, "该角色名已被占用");
        }
        TbRole tbRole = new TbRole();
        BeanUtils.copyProperties(roleVO, tbRole);
        tbRole.setCreateTime(new Date());
        tbRole.setModifiedTime(new Date());
        tbRole.setStatus(RoleStatusEnum.AVAILABLE.getStatusCode());
        roleDao.insert(tbRole);
    }


    /**
     * 删除角色
     *
     * @param id
     */
    @Transactional
    @Override
    public void deleteRole(Long id) throws SystemException {
        TbRole tbRole = roleDao.selectById(id);
        if (tbRole == null) {
            // 删除的角色不存在
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR, "要删除的角色不存在");
        }
        roleDao.deleteById(id);
        // 这里还需要删除对应的角色授权的菜单
    }

    @Override
    public RoleVO edit(Long id) throws SystemException {
        TbRole tbRole = roleDao.selectById(id);
        if (tbRole == null) {
            // 编辑的角色不存在
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR, "编辑的角色不存在");
        }
        RoleVO roleVO = new RoleVO();
        BeanUtils.copyProperties(tbRole, roleVO);
        return roleVO;
    }

    @Transactional
    @Override
    public void updateOneRole(Long id, RoleVO roleVO) throws SystemException {
        @NotBlank(message = "角色名必填") String roleName = roleVO.getRoleName();
        TbRole tbRole = roleDao.selectById(id);
        if (tbRole == null) {
            // 编辑的角色不存在
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR, "编辑的角色不存在");
        }
        // 判断编辑的角色是否存在
        TbRole role = roleDao.selectOne(new QueryWrapper<TbRole>().eq("role_name", roleName));
        if (role != null) {
            // 当前角色名已经存在
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR, "该角色名已被占用");
        }
        TbRole myRole = new TbRole();
        BeanUtils.copyProperties(roleVO, myRole);
        myRole.setModifiedTime(new Date());
        roleDao.updateById(myRole);
    }

    @Override
    public void updateRoleStatus(Long id, Boolean status) throws SystemException {
        TbRole tbRole = roleDao.selectById(id);
        if (tbRole == null) {
            // 删除的角色不存在
            throw new SystemException(SystemCodeEnum.PARAMETER_ERROR, "要更新的角色不存在");
        }
        Integer roleStatus;
        if (status == true) {
            // 启用
            roleStatus = RoleStatusEnum.AVAILABLE.getStatusCode();
        } else {
            roleStatus = RoleStatusEnum.DISABLE.getStatusCode();
        }
        roleDao.update(null, new UpdateWrapper<TbRole>().eq("id", id).set("status",roleStatus));
    }
}
