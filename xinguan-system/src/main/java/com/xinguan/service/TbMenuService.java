package com.xinguan.service;

import com.xinguan.entity.TbMenu;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-22
 */
public interface TbMenuService extends IService<TbMenu> {

}
