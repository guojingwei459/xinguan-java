package com.xinguan.service;

import com.xinguan.entity.TbDepartment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import com.xinguan.vo.system.DepartmentVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-11
 */
public interface TbDepartmentService extends IService<TbDepartment> {

    ResponseBean findDepartmentList(Integer currentPage, Integer pageSize, String departmentName);

    void addDepartment(DepartmentVO departmentVO) throws SystemException;

    void deleteDepartment(Long id) throws SystemException;

    DepartmentVO edit(Long id) throws SystemException;

    void updateDepartment(Long id, DepartmentVO departmentVO) throws SystemException;

    ResponseBean findAllDepartmentNames();


}
