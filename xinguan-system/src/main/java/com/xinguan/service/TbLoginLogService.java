package com.xinguan.service;

import com.xinguan.entity.TbLoginLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 登录日志表 服务类
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-24
 */
public interface TbLoginLogService extends IService<TbLoginLog> {

}
