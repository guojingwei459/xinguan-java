package com.xinguan.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinguan.entity.TbRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinguan.vo.system.RoleVO;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-11
 */
public interface TbRoleDao extends BaseMapper<TbRole> {


    IPage<RoleVO> selectRolePageVo(Page<?> page,@Param("roleName") String roleName);

}
