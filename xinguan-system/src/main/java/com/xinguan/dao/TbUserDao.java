package com.xinguan.dao;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xinguan.entity.TbUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinguan.vo.system.UserVO;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-10
 */
public interface TbUserDao extends BaseMapper<TbUser> {


    IPage<UserVO> findUserVOList(Page<?> page,UserVO userVO);
}
