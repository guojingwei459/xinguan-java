package com.xinguan.dao;

import com.xinguan.entity.TbUserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 用户角色关联表 Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-17
 */
public interface TbUserRoleDao extends BaseMapper<TbUserRole> {



    Integer batchInsert(List<TbUserRole> userRoles);




}
