package com.xinguan.dao;

import com.xinguan.entity.TbDepartment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-11
 */
public interface TbDepartmentDao extends BaseMapper<TbDepartment> {


}
