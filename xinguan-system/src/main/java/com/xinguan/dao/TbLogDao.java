package com.xinguan.dao;

import com.xinguan.entity.TbLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 操作日志表 Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-24
 */
public interface TbLogDao extends BaseMapper<TbLog> {

}
