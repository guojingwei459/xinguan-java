package com.xinguan.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinguan.entity.TbLoginLog;

/**
 * <p>
 * 登录日志表 Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-24
 */
public interface TbLoginLogDao extends BaseMapper<TbLoginLog> {

}
