package com.xinguan.dao;

import com.xinguan.entity.TbMenu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author 郭经伟
 * @since 2021-07-22
 */
public interface TbMenuDao extends BaseMapper<TbMenu> {

}
