package com.xinguan.error;

/**
 * @author 郭经伟
 * @Date 2021/7/11
 * 自定义异常
 **/
public class SystemException extends Exception implements BaseError {

    //所有实现了BaseError的ErrorEnum
    private BaseError baseError;

    public SystemException(BaseError baseError) {
        super(baseError.getErrorMsg());
        this.baseError = baseError;
    }

    public SystemException(BaseError baseError, String customErrorMessage) {
        super(customErrorMessage);
        this.baseError = baseError;
        this.baseError.setErrorMsg(customErrorMessage);
    }


    @Override
    public int getErrorCode() {
        return this.baseError.getErrorCode();
    }

    @Override
    public String getErrorMsg() {
        return this.baseError.getErrorMsg();
    }

    @Override
    public BaseError setErrorMsg(String message) {
        this.setErrorMsg(message);
        return this;
    }
}
