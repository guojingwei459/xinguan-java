package com.xinguan.response;

import lombok.Data;

/**
 * @author 郭经伟
 * @Date 2021/7/10
 * 全局返回
 **/
@Data
public class ResponseBean<T> {


    /**
     * 200：操作成功 -1：操作失败
     **/

    // http状态码
    private boolean success;

    // 返回数据
    private T data;


    public static <T> ResponseBean<T> success(T data) {
        ResponseBean<T> responseBean=new ResponseBean<>();
        responseBean.setSuccess(true);
        responseBean.setData(data);
        return responseBean;
    }

    public static <T> ResponseBean<T> error(T errorData){
        ResponseBean<T> responseBean=new ResponseBean<>();
        responseBean.setSuccess(false);
        responseBean.setData(errorData);
        return responseBean;
    }

    public static <T> ResponseBean<T> success() {
        ResponseBean<T> responseBean = new ResponseBean<>();
        responseBean.setSuccess(true);
        return responseBean;
    }
}
