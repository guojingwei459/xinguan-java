package com.xinguan.handler;

import com.xinguan.error.SystemException;
import com.xinguan.response.ResponseBean;
import org.apache.shiro.authz.UnauthorizedException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;

/**
 * @author 郭经伟
 * @Date 2021/7/11
 * 全局异常处理
 **/
@ControllerAdvice
public class GlobalExceptionHandler {


    private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);


    @ExceptionHandler(value = SystemException.class)
    @ResponseBody
    public ResponseBean systemExceptionHandler(HttpServletRequest request, SystemException e) {
        logger.error("系统模块-错误码：{},错误信息:{}", e.getErrorCode(), e.getErrorMsg());
        HashMap<String, Object> errorData = new HashMap<>();
        errorData.put("errorCode", e.getErrorCode());
        errorData.put("errorMsg", e.getErrorMsg());
        return ResponseBean.error(errorData);
    }


    @ExceptionHandler(value = UnauthorizedException.class)
    @ResponseBody
    public ResponseBean UnauthorizedException(HttpServletRequest request, UnauthorizedException unauthorizedException) {
        logger.info("系统模块-错误码：{},错误信息:{}", HttpStatus.UNAUTHORIZED.value(), unauthorizedException.getMessage());
        HashMap<String, Object> errorData = new HashMap<>();
        errorData.put("errorCode", HttpStatus.UNAUTHORIZED.value());
        errorData.put("errorMsg", "服务器向你抛了一个异常,并表示（操作无权限）");
        return ResponseBean.error(errorData);
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    @ResponseBody
    public ResponseBean httpRequestMethodNotSupportedException(HttpServletRequest request, HttpRequestMethodNotSupportedException httpRequestMethodNotSupportedException) {
        logger.info("系统模块-错误码：{},错误信息:{}", HttpStatus.BAD_REQUEST.value(), httpRequestMethodNotSupportedException.getMessage());
        HashMap<String, Object> errorData = new HashMap<>();
        errorData.put("errorCode", HttpStatus.BAD_REQUEST.value());
        errorData.put("errorMsg", "不支持该http请求方式");
        return ResponseBean.error(errorData);
    }

    @ExceptionHandler(value = NoHandlerFoundException.class)
    @ResponseBody
    public ResponseBean noHandlerFoundException(HttpServletRequest request, NoHandlerFoundException noHandlerFoundException) {
        logger.info("系统模块-错误码：{},错误信息:{}", HttpStatus.NOT_FOUND.value(), noHandlerFoundException.getMessage());
        HashMap<String, Object> errorData = new HashMap<>();
        errorData.put("errorCode", HttpStatus.NOT_FOUND.value());
        errorData.put("errorMsg", "服务器异常，请联系管理员");
        return ResponseBean.error(errorData);
    }

    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public ResponseBean otherException(HttpServletRequest request, Exception e) {
        logger.error("系统异常-错误码：{},错误信息:{}", HttpStatus.INTERNAL_SERVER_ERROR.value(), e.getMessage(), e);
        HashMap<String, Object> errorData = new HashMap<>();
        errorData.put("errorCode", HttpStatus.INTERNAL_SERVER_ERROR.value());
        errorData.put("errorMsg", "服务器异常，请联系管理员");
        return ResponseBean.error(errorData);
    }
}
