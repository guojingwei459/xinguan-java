package com.xinguan.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author 郭经伟
 * @Date 2021/7/24
 * 自定义注解 用于标注在controller的方法上，异步记录日志
 * 使用aop 异步实现日志
 **/
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface ControllerEndPoint {


    String operation() default "";

    String exceptionMessage() default "系统内部异常";
}
