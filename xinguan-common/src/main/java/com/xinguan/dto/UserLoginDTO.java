package com.xinguan.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;


/**
 * @author 郭经伟
 * @Date 2021/7/10
 * 用户登录dto
 **/
@Data
@ApiModel(value = "用户登录表单")
public class UserLoginDTO {


    @NotBlank(message = "用户名不能为空")
    @ApiModelProperty(value = "用户名")
    private String username;
    @NotBlank(message = "密码不能为空")
    @ApiModelProperty(value = "密码")
    private String password;
}
