package com.xinguan.enums.system;

/**
 * @author 郭经伟
 * @Date 2021/7/11
 **/
public enum RoleStatusEnum {


    DISABLE(0),
    AVAILABLE(1);

    private int statusCode;

    RoleStatusEnum(int statusCode) {
        this.statusCode = statusCode;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
