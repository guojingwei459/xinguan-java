package com.xinguan.enums.system;

/**
 * @author 郭经伟
 * @Date 2021/7/11
 **/
public enum UserTypeEnum {


    SYSTEM_ADMIN(0),
    SYSTEM_USER(1);

    private int typeCode;

    private UserTypeEnum(int typeCode) {
        this.typeCode = typeCode;
    }

    public int getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(int typeCode) {
        this.typeCode = typeCode;
    }
}
